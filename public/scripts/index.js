// Variables- HTML
const lblOnline = document.querySelector('#lblOnline');
const lblOffline = document.querySelector('#lblOffline');
const txtMensaje = document.querySelector('#txtMensaje');
const btnEnviar = document.querySelector('#btnEnviar');

// Variables- Socket
const socketCliente = io();

// MAIN
socketCliente.on('connect', () => {
    lblOffline.style.display = 'none';
});

socketCliente.on('disconnect', () => {
    lblOnline.style.display = 'none';
    lblOffline.style.display = '';
});

socketCliente.on('enviar-mensaje', (payload) => {
    console.log(payload);
});

btnEnviar.addEventListener('click', () => {
    const mensaje = txtMensaje.value;

    const payload = {
        mensaje,
        id: 'yydhHHDdhsyDHah12yS',
        fecha: new Date().getTime()
    }

    socketCliente.emit('enviar-mensaje', payload, (id) => {
       console.log(id);
    });
});