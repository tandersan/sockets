// Requires
const express = require('express');
const {socketController} = require('../controllers/ejercicio');
const cors = require('cors');
require('colors');

//MAIN
class Server {
    constructor() {
        this.app = express();
        this.puerto = process.env.PORT;
        this.server = require('http').createServer(this.app);
        this.io = require('socket.io')(this.server);
        this.publicPath = __dirname + '/../' + process.env.PUBLICPATH;
        this.routePaths = {}

        //Middlewares
        this.middlewares();

        // Sockets
        this.sockets();
    }

    middlewares() {
        // CORS
        this.app.use(cors());

        // Public Directory
        this.app.use(express.static(this.publicPath));
    }

    sockets() {
        this.io.on('connection', socketController);
    }

    subirServidor() {
        this.server.listen(this.puerto, () => {
            console.log(`\nServer On Line\nListening port : ${(this.puerto + '').yellow}`);
        });
    }

    async conectarBBDD() {
        await dbConexion();
    }
}

module.exports = Server;