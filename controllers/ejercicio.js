// MAIN
const socketController = (socketServer) => {
    console.log('Cliente conectado ', socketServer.id);

    socketServer.on('disconnect', () => {
    })

    socketServer.on('enviar-mensaje', (payload, callback) => {
        socketServer.broadcast.emit('enviar-mensaje', payload);
        callback('qwerty');
    })
}

module.exports = {
    socketController
}