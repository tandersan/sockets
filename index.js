// Requires
require('colors');
require('dotenv').config();

//Variables
const Server = require('./server/server');

//MAIN
const main = async () => {
    const server = new Server();

    server.subirServidor();
}

main();